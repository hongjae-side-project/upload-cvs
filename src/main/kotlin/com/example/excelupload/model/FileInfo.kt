package com.example.excelupload.model

class FileInfo {
    val filename: String = ""
    val url: String = ""
}