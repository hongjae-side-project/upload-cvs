package com.example.excelupload

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ExceluploadApplication

fun main(args: Array<String>) {
    runApplication<ExceluploadApplication>(*args)
}
