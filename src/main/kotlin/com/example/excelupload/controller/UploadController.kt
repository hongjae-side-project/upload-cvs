package com.example.excelupload.controller

import com.example.excelupload.filestorage.FileStorage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile

@Controller
class UploadController {

    @Autowired
    lateinit var fileStorage: FileStorage

    @GetMapping("/csvupload")
    fun mainView(model: Model): String {
        model.addAttribute("msg","UTF-8 Encoding CSV Upload")
        return "csvupload" // csvupload.html호출
    }

    @PostMapping("/csvupload")
    fun uploadFile(@RequestParam("uploadfile") file: MultipartFile, model: Model): String {
        fileStorage.store(file);
        model.addAttribute("message", file.getOriginalFilename()+" upload success!" )
        return "csvupload"
    }
}