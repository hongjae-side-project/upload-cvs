package com.example.excelupload.filestorage

import org.springframework.core.io.Resource
import org.springframework.web.multipart.MultipartFile
import java.util.stream.Stream


interface FileStorage {
    fun store(file: MultipartFile)
    fun loadFile(filename: String): Resource
    fun deleteAll()
    fun init()
    fun loadFiles(): Stream<*>
}