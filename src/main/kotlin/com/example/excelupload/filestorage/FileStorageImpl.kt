package com.example.excelupload.filestorage;

import mu.KotlinLogging
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import java.nio.file.StandardCopyOption

@Service
class FileStorageImpl: FileStorage{
    private val logger = KotlinLogging.logger {}

    val rootLocation = Paths.get("uploadfiles")

    override fun store(file: MultipartFile){
        logger.debug { "test1" }
        logger.debug { file.originalFilename }
        logger.debug { "test2" }
        Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING)
    }

    override fun loadFile(filename: String): Resource{
        val file = rootLocation.resolve(filename)
        val resource = UrlResource(file.toUri())

        if(resource.exists() || resource.isReadable()) {
            return resource
        }else{
            throw RuntimeException("FAIL!")
        }
    }

    override fun deleteAll(){
        FileSystemUtils.deleteRecursively(rootLocation.toFile())
    }

    override fun init(){
        Files.createDirectory(rootLocation)
    }

    override fun loadFiles(): Stream<*>{
        return Files.walk(this.rootLocation, 1)
            .filter{path -> !path.equals(this.rootLocation)}
            .map(this.rootLocation::relativize)
    }
}